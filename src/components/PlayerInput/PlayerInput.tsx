import * as React from 'react'

import Label from '$components/Label/Label'
import Input from '$components/Input/Input'

interface Props {
  label: string
  inputId: string
  onChange: (text: string) => void
}

const PlayerInput: React.SFC<Props> = props => {
  return (
    <div>
      <Label htmlFor={props.inputId}>{props.label}</Label>
      <Input
        type="text"
        id={props.inputId}
        onChange={event => props.onChange(event.currentTarget.value)}
      />
    </div>
  )
}

export default PlayerInput
