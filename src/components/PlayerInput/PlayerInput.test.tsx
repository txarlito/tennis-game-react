import * as React from 'react'
import * as ReactDOM from 'react-dom'
import * as renderer from 'react-test-renderer'
import * as enzyme from 'enzyme'

import PlayerInput from '$components/PlayerInput/PlayerInput'

describe('PlayerInput', () => {
  it('renders without crashing', () => {
    const div = document.createElement('div')
    ReactDOM.render(
      <PlayerInput
        label="Player 1"
        inputId="player-1"
        onChange={() => {
          return
        }}
      />,
      div,
    )
    ReactDOM.unmountComponentAtNode(div)
  })

  it('renders correctly', () => {
    const tree = renderer
      .create(
        <PlayerInput
          label="Player 1"
          inputId="player-1"
          onChange={() => {
            return
          }}
        />,
      )
      .toJSON()

    expect(tree).toMatchSnapshot()
  })

  it('calls onChange prop when changed', () => {
    const onChangeMock = jest.fn()
    const component = enzyme.mount(
      <PlayerInput
        label="Player 1"
        inputId="player-1"
        onChange={onChangeMock}
      />,
    )

    component.find('input').simulate('change')
    expect(onChangeMock).toBeCalled()
  })

  it('shows player name in label', () => {
    const component = enzyme.mount(
      <PlayerInput
        label="Player 1"
        inputId="player-1"
        onChange={() => {
          return
        }}
      />,
    )
    const labelText = component.find('label').text()

    expect(labelText).toContain('Player 1')
  })
})
