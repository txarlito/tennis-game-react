import * as React from 'react'

import PlayerButton from '$components/PlayerButton/PlayerButton'
import Scoreboard from '$components/Scoreboard/Scoreboard'
import Button from '$components/Button/Button'
import GameGrid from '$components/GameGrid/GameGrid'
import GameGridItem from '$components/GameGridItem/GameGridItem'

interface Props {
  player1Name: string
  player2Name: string
  scoreHistory: string[]
  isFinished: boolean
  onWonPointClick: (playerName: string) => void
  onNewGameClick: () => void
}

const GameView: React.SFC<Props> = props => {
  return (
    <GameGrid>
      <GameGridItem>
        <PlayerButton
          playerName={props.player1Name}
          onClick={() => props.onWonPointClick(props.player1Name)}
          disabled={props.isFinished}
        />
      </GameGridItem>
      <GameGridItem lastOnMobile={true} alignToEnd={true}>
        <Scoreboard scoreHistory={props.scoreHistory} />
        <Button hidden={!props.isFinished} onClick={props.onNewGameClick}>
          New game
        </Button>
      </GameGridItem>
      <GameGridItem>
        <PlayerButton
          playerName={props.player2Name}
          onClick={() => props.onWonPointClick(props.player2Name)}
          disabled={props.isFinished}
        />
      </GameGridItem>
    </GameGrid>
  )
}

export default GameView
