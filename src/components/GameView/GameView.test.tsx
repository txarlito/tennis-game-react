import * as React from 'react'
import * as ReactDOM from 'react-dom'
import GameView from '$components/GameView/GameView'

describe('GameView', () => {
  it('renders without crashing', () => {
    const div = document.createElement('div')
    ReactDOM.render(
      <GameView
        isFinished={false}
        onNewGameClick={() => {
          return
        }}
        onWonPointClick={() => {
          return
        }}
        player1Name="Nadal"
        player2Name="Federer"
        scoreHistory={[]}
      />,
      div,
    )
    ReactDOM.unmountComponentAtNode(div)
  })
})
