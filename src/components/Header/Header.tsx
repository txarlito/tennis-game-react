import styled from 'styled-components'
import * as React from 'react'

const HeaderStyled = styled.header`
  padding: 1.5em 0;

  text-align: center;
`

const Header: React.SFC<{}> = props => {
  return (
    <HeaderStyled>
      <h1>{props.children}</h1>
    </HeaderStyled>
  )
}

export default Header
