import styled from 'styled-components'

const Label = styled.label`
  display: block;
  padding: 0.75em;

  font-weight: bold;
`

export default Label
