import styled from 'styled-components'

const Button = styled.button`
  padding: 0.75em;

  font-family: 'Raleway', sans-serif;
  font-size: 1.1em;
  font-weight: bold;
  color: black;

  border: 2px black solid;
  background-color: #ffffff;

  &:hover {
    margin: -1px;

    border: 3px black solid;
  }

  &:disabled {
    margin: 0;

    color: grey;
    border: 1px grey solid;
  }
`

export default Button
