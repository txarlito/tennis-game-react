import * as React from 'react'

import '../../sass/variables.scss'

import Button from '$components/Button/Button'
import PlayerName from '$components/PlayerName/PlayerName'

interface Props {
  playerName: string
  disabled: boolean
  onClick: () => void
}

const PlayerButton: React.SFC<Props> = props => (
  <>
    <PlayerName>{props.playerName}</PlayerName>
    <Button onClick={props.onClick} disabled={props.disabled}>
      Won Point
    </Button>
  </>
)

export default PlayerButton
