import * as React from 'react'
import * as ReactDOM from 'react-dom'
import PlayerButton from '$components/PlayerButton/PlayerButton'

describe('PlayerButton', () => {
  it('renders without crashing', () => {
    const div = document.createElement('div')
    ReactDOM.render(
      <PlayerButton
        playerName="Nadal"
        disabled={false}
        onClick={() => {
          return
        }}
      />,
      div,
    )
    ReactDOM.unmountComponentAtNode(div)
  })
})
