import * as React from 'react'

import TennisGame from '../../lib/tennis-game'

import Header from '$components/Header/Header'
import GameStartView from '$components/GameStartView/GameStartView'
import GameView from '$components/GameView/GameView'

enum Page {
  gameStart,
  game,
}

interface State {
  currentPage: Page
  player1Name: string
  player2Name: string
  scoreHistory: string[]
}

class App extends React.Component<{}, State> {
  state = {
    currentPage: Page.gameStart,
    player1Name: '',
    player2Name: '',
    scoreHistory: [],
  }

  private tennisGame: TennisGame

  render() {
    let viewToShow

    switch (this.state.currentPage) {
      default:
      case Page.gameStart:
        viewToShow = (
          <GameStartView
            onGameInitialization={(player1Name, player2Name) =>
              this.handleGameInitialization(player1Name, player2Name)
            }
          />
        )
        break
      case Page.game:
        viewToShow = (
          <GameView
            player1Name={this.state.player1Name}
            player2Name={this.state.player2Name}
            scoreHistory={this.state.scoreHistory}
            isFinished={this.tennisGame.isFinished()}
            onWonPointClick={playerName => this.handleWonPointClick(playerName)}
            onNewGameClick={() => this.handleNewGameClick()}
          />
        )
        break
    }

    return (
      <div>
        <Header>Tennis Game</Header>

        {viewToShow}
      </div>
    )
  }

  private handleGameInitialization = (
    player1Name: string,
    player2Name: string,
  ) => {
    this.tennisGame = new TennisGame(player1Name, player2Name)

    this.saveScoreToHistory()
    this.setState({
      player1Name: player1Name,
      player2Name: player2Name,
      currentPage: Page.game,
    })
  }

  private handleWonPointClick = (playerName: string) => {
    if (this.tennisGame.isFinished()) {
      return
    }

    this.tennisGame.wonPoint(playerName)
    this.saveScoreToHistory()
  }

  private handleNewGameClick = () => {
    this.restartGame()
  }

  private saveScoreToHistory() {
    const scoreHistory = [
      ...this.state.scoreHistory,
      this.tennisGame.getScore(),
    ]

    this.setState({
      scoreHistory: scoreHistory,
    })
  }

  private restartGame = () => {
    this.setState({
      currentPage: Page.gameStart,
      player1Name: '',
      player2Name: '',
      scoreHistory: [],
    })
  }
}

export default App
