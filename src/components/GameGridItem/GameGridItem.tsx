// import * as React from 'react'
import styled from 'styled-components'
import '../../sass/variables.scss'

interface Props {
  lastOnMobile?: boolean
  alignToEnd?: boolean
}

const GameGridItem = styled<Props, 'div'>('div')`
  text-align: center;

  align-self: ${props => (props.lastOnMobile ? 'flex-end' : null)};

  @media screen and (max-width: $mobile-width) {
    width: 100%;

    order: ${props => (props.lastOnMobile ? 1 : null)};
    align-self: ${props => (props.lastOnMobile ? 'center' : null)};
  }
`

export default GameGridItem
