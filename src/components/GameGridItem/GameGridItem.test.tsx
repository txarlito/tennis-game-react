import * as React from 'react'
import * as ReactDOM from 'react-dom'
import GameGridItem from '$components/GameGridItem/GameGridItem'

describe('GameGridItem', () => {
  it('renders without crashing', () => {
    const div = document.createElement('div')
    ReactDOM.render(<GameGridItem />, div)
    ReactDOM.unmountComponentAtNode(div)
  })
})
