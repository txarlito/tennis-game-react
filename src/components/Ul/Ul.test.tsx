import * as React from 'react'
import * as ReactDOM from 'react-dom'
import * as renderer from 'react-test-renderer'

import Ul from '$components/Ul/Ul'

describe('Ul', () => {
  it('renders without crashing', () => {
    const div = document.createElement('div')
    ReactDOM.render(<Ul />, div)
    ReactDOM.unmountComponentAtNode(div)
  })

  it('renders correctly', () => {
    const tree = renderer.create(<Ul />).toJSON()
    expect(tree).toMatchSnapshot()
  })
})
