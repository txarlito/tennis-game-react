import styled from 'styled-components'

const Ul = styled.ul`
  padding: 0;
  margin-bottom: 2em;

  list-style: none;
`

export default Ul
