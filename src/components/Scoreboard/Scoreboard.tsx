import * as React from 'react'

import Ul from '$components/Ul/Ul'

interface Props {
  scoreHistory: string[]
}

const Scoreboard: React.SFC<Props> = props => {
  return (
    <Ul>
      {props.scoreHistory.map((score, index) => {
        return <li key={index}>{score}</li>
      })}
    </Ul>
  )
}

export default Scoreboard
