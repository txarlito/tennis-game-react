import * as React from 'react'
import * as ReactDOM from 'react-dom'
import * as renderer from 'react-test-renderer'

import Scoreboard from '$components/Scoreboard/Scoreboard'

describe('Scoreboard', () => {
  it('renders without crashing', () => {
    const scoreHistory = ['Love All', 'Love - Fifteen']

    const div = document.createElement('div')
    ReactDOM.render(<Scoreboard scoreHistory={scoreHistory} />, div)
    ReactDOM.unmountComponentAtNode(div)
  })

  it('renders correctly', () => {
    const scoreHistory = ['Love All', 'Love - Fifteen']

    const tree = renderer
      .create(<Scoreboard scoreHistory={scoreHistory} />)
      .toJSON()

    expect(tree).toMatchSnapshot()
  })
})
