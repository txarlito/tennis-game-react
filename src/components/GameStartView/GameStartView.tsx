import * as React from 'react'

import PlayerInput from '$components/PlayerInput/PlayerInput'
import GameGrid from '$components/GameGrid/GameGrid'
import GameGridItem from '$components/GameGridItem/GameGridItem'
import Button from '$components/Button/Button'

interface Props {
  onGameInitialization: (player1Name: string, player2Name: string) => void
}

interface State {
  player1Name: string
  player2Name: string
}

class GameStartView extends React.Component<Props, State> {
  state = {
    player1Name: null,
    player2Name: null,
  }

  render() {
    return (
      <GameGrid>
        <GameGridItem>
          <PlayerInput
            label="Player 1"
            inputId="player-1"
            onChange={text => {
              this.setState({ player1Name: text })
            }}
          />
        </GameGridItem>
        <GameGridItem lastOnMobile={true} alignToEnd={true}>
          <Button
            disabled={!this.state.player1Name || !this.state.player2Name}
            onClick={this.handleClick}
          >
            Play!
          </Button>
        </GameGridItem>
        <GameGridItem>
          <PlayerInput
            label="Player 2"
            inputId="player-2"
            onChange={text => {
              this.setState({ player2Name: text })
            }}
          />
        </GameGridItem>
      </GameGrid>
    )
  }

  private handleClick = () => {
    this.props.onGameInitialization(
      this.state.player1Name,
      this.state.player2Name,
    )
  }
}

export default GameStartView
