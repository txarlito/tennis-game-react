import * as React from 'react'
import * as ReactDOM from 'react-dom'
import GameStartView from '$components/GameStartView/GameStartView'

describe('GameStartView', () => {
  it('renders without crashing', () => {
    const div = document.createElement('div')
    ReactDOM.render(
      <GameStartView
        onGameInitialization={() => {
          return
        }}
      />,
      div,
    )
    ReactDOM.unmountComponentAtNode(div)
  })
})
