import styled from 'styled-components'

const GameGrid = styled.div`
  display: flex;
  width: 100%;
  height: 20em;

  flex-direction: row;
  align-items: center;
  justify-content: space-around;

  @media screen and (max-width: $mobile-width) {
    flex-direction: column;
  }
`

export default GameGrid
