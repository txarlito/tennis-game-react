import * as React from 'react'
import * as ReactDOM from 'react-dom'
import GameGrid from '$components/GameGrid/GameGrid'

describe('GameGrid', () => {
  it('renders without crashing', () => {
    const div = document.createElement('div')
    ReactDOM.render(<GameGrid />, div)
    ReactDOM.unmountComponentAtNode(div)
  })
})
