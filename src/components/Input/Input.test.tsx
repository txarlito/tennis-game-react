import * as React from 'react'
import * as ReactDOM from 'react-dom'
import * as renderer from 'react-test-renderer'

import Input from '$components/Input/Input'

describe('Input', () => {
  it('renders without crashing', () => {
    const div = document.createElement('div')
    ReactDOM.render(<Input />, div)
    ReactDOM.unmountComponentAtNode(div)
  })

  it('renders correctly', () => {
    const tree = renderer.create(<Input />).toJSON()
    expect(tree).toMatchSnapshot()
  })
})
