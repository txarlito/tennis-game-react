import styled from 'styled-components'

const Input = styled.input`
  padding: 0.75em;
`

export default Input
