import * as React from 'react'
import * as ReactDOM from 'react-dom'
import PlayerName from '$components/PlayerName/PlayerName'

describe('PlayerName', () => {
  it('renders without crashing', () => {
    const div = document.createElement('div')
    ReactDOM.render(<PlayerName />, div)
    ReactDOM.unmountComponentAtNode(div)
  })
})
