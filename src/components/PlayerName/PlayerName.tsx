import styled from 'styled-components'

const PlayerName = styled.span`
  display: block;
  padding: 1em;

  font-weight: bold;

  @media screen and (max-width: $mobile-width) {
    display: inline;
  }
`

export default PlayerName
